﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockTrading
{
    class rates
    {
        
        public string Item { get; set; }
        public double Rates { get; set; }
        public static ObservableCollection<rates> fetchData() {
            var data = new ObservableCollection<rates>();
            data.Add(new rates() { Item = "USD", Rates=250.00});
            data.Add(new rates() { Item = "BRTN", Rates = 3000.00 });
            data.Add(new rates() { Item = "PKR", Rates = 150.00 });
            data.Add(new rates() { Item = "IND", Rates = 50.88 });
            data.Add(new rates() { Item = "BAN", Rates = 1500.00 });
            data.Add(new rates() { Item = "SRI", Rates = 250.00 });
            data.Add(new rates() { Item = "DUB", Rates = 37.00 });
            data.Add(new rates() { Item = "QAT", Rates = 5000.00 });


            return data;
        }
    }
}
